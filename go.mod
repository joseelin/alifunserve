module gitee.com/joseelin/alifunserve

go 1.12

require (
	github.com/awesome-fc/golang-runtime v0.0.0-20190923032604-518f4689427d // indirect
	github.com/sirupsen/logrus v1.5.0 // indirect
	github.com/t-tomalak/logrus-easy-formatter v0.0.0-20190827215021-c074f06c5816 // indirect
)
